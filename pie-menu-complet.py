# todo : Mettre l'édition proportionnlle'
# todo: mette le edit mode / object mode
import bpy
from bpy.types import Menu

bl_info = {
    "name": "Pie-menu complet",
    "author": "Sandy",
    "version": (0, 1),
    "blender": (2, 80, 0),
    "location": "Partout",
    "description": "Pie menu complet",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Pie Menu",
}

# spawn an edit mode selection pie (run while object is in edit mode to get a valid output)


class VIEW3D_MT_PIE_template(Menu):
    # label is displayed at the center of the pie menu.
    bl_label = "Select Mode"
    bl_idname = "Mon Menu"

    def draw(self, context):
        layout = self.layout

        pie = layout.menu_pie()
        # operator_enum will just spread all available options
        # for the type enum of the operator on the pie
        pie.operator_enum("mesh.select_mode", "type")


def register():
    bpy.utils.register_class(VIEW3D_MT_PIE_template)
    wm = bpy.context.window_manager

    km = wm.keyconfigs.addon.keymaps.new(name='3D View', space_type="VIEW_3D")
    kmi = km.keymap_items.new('wm.call_menu_pie', 'Q', 'PRESS').properties.name = "Mon Menu"

def unregister():
    bpy.utils.unregister_class(VIEW3D_MT_PIE_template)


if __name__ == "__main__":
    register()

    bpy.ops.wm.call_menu_pie(name="VIEW3D_MT_PIE_template")
